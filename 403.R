### This is a snippet of the mtcars dataset that shows the miles per gallon
### (mpg) and horsepower (hp) data. If you wanted to compare the mpg and hp
### values against each other, what kind of plot would you use?

### Scatter plots are designed to show how one set of data relates to another.
### Bar and pie plots only use one data set, and box plots require multiple y
### points for each x location.

### Examine the Wilcoxon test statistic for x and y+10 and for x and y+100.
### Because the Wilcoxon works on ranks, once the two groups show complete
### separation, that is all points from group y are above all points from group
### x, the statistic will not change, regardless of how large the difference
### grows. Likewise, the p-value has a minimum value, regardless of how far
### apart the groups are. This means that the Wilcoxon test can be considered
### less powerful than the t-test in certain contexts. In fact, for small sample
### sizes, the p-value can't be very small, even when the difference is very
### large.

### What is the p-value if we compare c(1,2,3) to c(4,5,6) using a Wilcoxon
### test?
wilcox.test(c(1, 2, 3), c(4, 5, 6)) # 0.1

### What is the p-value if we compare c(1,2,3) to c(400,500,600) using a
### Wilcoxon test?
wilcox.test(c(1, 2, 3), c(400, 500, 600))$p.value # 0.1

### Load the NYC marathon data used in a previous assessment, and create a
### vector time of the sorted times:

data(nym.2002, package="UsingR")
time = sort(nym.2002$time)

### Compare the following two plots.

### 1) A plot of the ratio of times to the median time, with horizontal lines at
### twice as fast as the median time, and twice as slow as the median time.

plot(time / median(time), ylim = c(1 / 4, 4))
abline(h = c(1 / 2, 1, 2))

### 2) A plot of the log2 ratio of times to the median time. The horizontal
### lines indicate the same as above: twice as fast and twice as slow.

plot(log2(time / median(time)), ylim = c(-2, 2))
abline(h = -1:1)

### Note that the lines are equally spaced in Figure #2.

### The logarithm of a ratio (or a division, or a fraction) can always be
### expressed as a difference of two logarithms.
